# Write many mails!

Sometimes you have to write a lot of similar, but not identical, mails to different persons. When (B)CC is not an
option, this project may help you.

## Installation

The package is built and hosted on gitlab. The simplest way to use it is to install it via pipx:

```
pipx install template_mailer --index-url https://gitlab.com/api/v4/projects/40066196/packages/pypi/simple
```

If you want to hack on the project, clone this repository and run `poetry install`.

## Usage

### Commandline parameters

The program is invoked via `python -m template_mailer <data file> <template file>` and the following options may be used

- `<data file>`: The data to be used in the mail. Either a csv file or a json file containing a list of dicts.
- `<template file>`: A [jinja2][jinja2] template from which the mails are rendered. More on this later.
- `--wet`: Enables actual sending of the mails. The default is only to "dry run", that is showing the rendered mails on
  standard out put sending them.
- `--mail`: The column from which the address where the mails are sent to is obtained. Defaults to "mail".
- `--subject`: The column from which the used subject is obtained. Defaults to "subject".
- `--config-file`: The file that configures the outgoing mail server. More on this later. Defaults
  to `${XDG_CONFIG_DIR}/mail.config`.
- `--config`: The section to use in the config file. Defaults to the first one.
- `--attachment`: Attachment to add to _each_ mail. Can be specified multiple times.
- `--ccs`: CCs to add to _each_ mail. Can be specified multiple times.
- `--bccs`: BCCs to add to _each_ mail. Can be specified multiple times.

### Config file

The outgoing mail server is configured by a file in the [`.ini` format][ini]. The following options are supported (they
are case-insensitive):

- `Server`: The address of the smtp-server to use. This often is `mail.provider.com` or `smtp.provider.com`.
- `User`: The username to use for login on the server. This often is the address you want to send from.
- `Port`: Port to use on the server. It is optional and defaults to `587`.
- `PassPath`: If the password to the mail account is stored in a [pass][pass] repository, provide its path and the
  program will read it from there. Otherwise, the password is queried on the commandline when running.

### Template file

The mail is given as a [jinja2][jinja2] template file. It is rendered once for each row of the input data. All entries
of the row are available in the template under their column name. For more info on how to write templates see [the documentation][jinja2].

#### Setting variables in the templates

You can set variables in a template for later use. For the variables `mail`, `subject`, `attachments`, `bccs` and `ccs`, this has the additional effect to set the configuration options. In particular, you can make them depend on the current row which the command line does not allow.

#### Example

- file: data.csv

```csv
name,score,id
alice,98,12345
bob,33,98765
```

- file: mail_template.mail

```jinja2
{% set subject="Your test score, " + name %}
{% set mail="{}@example_uni.org".format(id) %}
Hi {{name}},

you scored {{score}}% on the last test. {% if score | int >= 50 -%}
    This means you passed, congratulations
{% else -%}
    This means you failed, you can try again next semester.
{% endif %}

Best Wishes,
Someone
```

Running `python -m template_mailer data.csv mail_template.mail` gives the following output:

```
Running dry: To: 12345@example_uni.org, Subject: Your test score, alice
Hi alice,

you scored 98% on the last test. This means you passed, congratulations

Best Wishes,
Someone
--------------------
Running dry: To: 98765@example_uni.org, Subject: Your test score, bob
Hi bob,

you scored 33% on the last test. This means you failed, you can try again next semester.

Best Wishes,
Someone
--------------------
```

# Details

The jinja2 environment is defined with the arguments

```python
jinja2.Environment(
    variable_start_string="{{",
    variable_end_string="}}",
    line_comment_prefix="%#",
    line_statement_prefix="%%",
    trim_blocks=True,
    autoescape=False,
    loader=jinja2.FileSystemLoader("."),
)
```

this is not (yet) customizable.

[ini]: https://docs.python.org/3/library/configparser.html#supported-ini-file-structure
[jinja2]: https://jinja.palletsprojects.com/en/3.0.x/
[pass]: https://jinja.palletsprojects.com/en/3.0.x/
