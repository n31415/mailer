import json
import time
from pathlib import Path
from typing import Any

import jinja2

from template_mailer.Entry import Entry
from template_mailer.Mailbox import BaseMailbox, IMAPMailbox, NoopMailbox

from .Arguments import Arguments, parse_args
from .BoundTemplate import BoundTemplate, get_env
from .Configuration import Configuration, get_config
from .DataReader import DataType, read
from .Mailer import BaseMailer, DryMailer, Mailer
from .Message import Message
from .TemplateHelpers import template_globals


class Runner:
    arguments: Arguments
    config: Configuration
    data: DataType

    def __init__(self, arguments: Arguments):
        self.arguments = arguments
        self.config = get_config(arguments.config, arguments.config_file)
        self.data = read(self.arguments.data)

    def run(self) -> None:
        mailer = Mailer(self.config) if self.arguments.smtp else DryMailer(self.config)
        mailbox = IMAPMailbox(self.config) if self.arguments.imap else NoopMailbox()

        env = get_env()
        index = 0
        for index, row in enumerate(self.data):
            if self.arguments.sleep and index > 0:
                time.sleep(self.arguments.sleep)
            try:
                self._handle_row(row, env, mailer, mailbox)
            except Exception:
                missing = self.data[index:]
                with Path("failure.json").open("w") as f:
                    json.dump(missing, f)
                raise

    def _handle_row(
        self,
        row: dict[str, Any],
        env: jinja2.Environment,
        mailer: BaseMailer,
        mailbox: BaseMailbox,
        **kwargs: Any,
    ) -> None:
        def unwrap_entry(arg: Entry | str) -> str:
            if isinstance(arg, Entry):
                return str(arg.extract(row))
            return arg

        template = self._bind_template(env, row)
        found_arguments = template.arguments
        if found_arguments.subject is None:
            raise RuntimeError("You need to provide `subject` via the commandline or in the email template.")

        message = Message(
            to_addr=unwrap_entry(found_arguments.mail),
            subject=unwrap_entry(found_arguments.subject),
            message=str(template),
            from_addr=self.config.sender,
            attachments=found_arguments.attachments,
            CCs=found_arguments.ccs,
            BCCs=found_arguments.bccs,
            **kwargs,
        )

        print(mailer.send_msg(message, retry=True))
        print("-" * 20)
        mailbox.store(message)

    def _bind_template(self, env: jinja2.Environment, row: dict[str, Any]) -> BoundTemplate:
        return BoundTemplate(
            env.get_template(str(self.arguments.template), globals=template_globals | {"arguments": self.arguments}),
            arguments=self.arguments,
            parameters=row,
        )


def run() -> None:
    arguments = parse_args()
    runner = Runner(arguments)
    runner.run()
