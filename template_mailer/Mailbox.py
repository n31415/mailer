import dataclasses
import datetime
import imaplib
import json
import pathlib
from typing import Protocol, override

from template_mailer.Configuration import Configuration
from template_mailer.Message import Message
from template_mailer.Password import get_password


def create_server(config: Configuration) -> imaplib.IMAP4_SSL:
    host = config.imap_server or config.server
    try:
        return imaplib.IMAP4_SSL(host=host, port=config.imap_port)
    except ConnectionRefusedError as e:
        port = config.imap_port
        e.add_note(f"{host=}, {port=}")
        raise


class BaseMailbox(Protocol):
    def store(self, msg: Message) -> None:
        ...


class IMAPMailbox(BaseMailbox):
    def __init__(self, config: Configuration) -> None:
        self.config = config

        self.password = get_password(config)
        self.server = create_server(config)
        self.server.login(self.config.user, self.password)

    @override
    def store(self, msg: Message) -> None:
        self.server.append(
            self.config.imap_mailbox,
            "\\Seen",
            imaplib.Time2Internaldate(datetime.datetime.now().astimezone()),
            msg.to_msg(self.config.sender).as_bytes(),
        )


class NoopMailbox(BaseMailbox):
    @override
    def store(self, msg: Message) -> None:
        _ = msg


class JsonMailbox(BaseMailbox):
    def __init__(self, path: pathlib.Path):
        self.path = path
        self.path.mkdir(parents=True, exist_ok=True)

    def _filepath(self, msg: Message) -> pathlib.Path:
        to = msg.to_addr if isinstance(msg.to_addr, str) else "_".join(msg.to_addr)
        identifier = msg.subject + "_TO_" + to + ".json"
        return self.path / identifier

    @override
    def store(self, msg: Message) -> None:
        with self._filepath(msg).open("w") as fp:
            json.dump(dataclasses.asdict(msg), fp)
