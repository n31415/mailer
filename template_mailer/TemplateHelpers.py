import pathlib
from collections.abc import Callable
from functools import cache
from typing import Any, TypeVar

from template_mailer.DataReader import read

template_globals = {}
T = TypeVar("T", bound=Callable[..., Any])


def _global(func: T) -> T:
    template_globals[func.__name__] = func
    return func


@_global
@cache
def load_data(path: str) -> Any:
    return read(pathlib.Path(path))
