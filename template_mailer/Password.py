import getpass
import os
import subprocess
from pathlib import Path

from .Configuration import Configuration


def from_stdin(msg: str = "Password?") -> str:
    return getpass.getpass(msg)


def from_pass(path: Path) -> str:
    env = os.environ | {"PINENTRY_USER_DATA": "USE_GRAPHICAL=1"}
    process = subprocess.run(["pass", path], text=True, capture_output=True, env=env, check=True)
    return process.stdout.rstrip()


def get_password(config: Configuration) -> str:
    if config.passpath is None:
        return from_stdin(f"Password for {config.user}")
    return from_pass(config.passpath)
