import configparser
from pathlib import Path

from pydantic import AliasChoices, AliasPath, BaseModel, Field


class Configuration(BaseModel):
    server: str
    user: str
    sender: str = Field(validation_alias=AliasChoices("sender", "from", AliasPath("user")))
    port: int = 587
    passpath: Path | None = None
    starttls: bool = False

    imap_server: str | None = None
    imap_user: str | None = None
    imap_port: int = 993
    imap_mailbox: str = "Sent"


def read_config(path: Path) -> configparser.ConfigParser:
    if not path.exists():
        raise FileNotFoundError(path)
    config_file = configparser.ConfigParser()
    config_file.read(path)
    default = config_file[config_file.default_section].get("DefaultSection", config_file.sections()[0])
    config_file.default_section = default

    return config_file


def get_config(name: str | None, path: Path) -> Configuration:
    config = read_config(path)

    if name is None:
        name = config.default_section

    return Configuration.model_validate(dict(config.items(name)))
