from typing import Any

import jinja2

from .Arguments import Arguments


def get_env(**kwargs: Any) -> jinja2.Environment:
    default_args = {
        "variable_start_string": "{{",
        "variable_end_string": "}}",
        "line_comment_prefix": "%#",
        "line_statement_prefix": "%%",
        "trim_blocks": True,
        "autoescape": False,
        "undefined": jinja2.StrictUndefined,
        "loader": jinja2.FileSystemLoader("."),
    }
    return jinja2.Environment(**(default_args | kwargs))  # type: ignore


class BoundTemplate:
    template_module: jinja2.environment.TemplateModule
    _passed_arguments: Arguments
    environment: jinja2.Environment

    def __init__(
        self,
        template: jinja2.Template,
        arguments: Arguments,
        parameters: dict[str, Any],
    ):
        self._passed_arguments = arguments
        self.template_module = template.make_module(vars=parameters | self._passed_arguments.settable_from_template())
        self.environment = template.environment
        self.parameters = parameters

    @property
    def arguments(self) -> Arguments:
        relevant_vars = {
            name: value
            for (name, value) in [
                (member, getattr(self.template_module, member, None))
                for member in self._passed_arguments.settable_from_template()
            ]
            if value is not None
        }
        updated = self._passed_arguments.model_dump() | relevant_vars
        return self._passed_arguments.model_validate(updated)

    def __str__(self) -> str:
        return str(self.template_module)

    def _render_string(self, string: str) -> str:
        return self.environment.from_string(string).render(**self.parameters)
