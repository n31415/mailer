import functools
from collections.abc import Iterable
from dataclasses import dataclass, field
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from pathlib import Path

COMMASPACE = ", "


def concatenate(inp: Iterable[str]) -> str:
    return COMMASPACE.join(inp)


@functools.cache
def read_attachment(path: Path) -> MIMEApplication:
    with path.open("rb") as fil:
        part = MIMEApplication(fil.read(), Name=path.name)

    part["Content-Disposition"] = f'attachment; filename="{path.name}"'
    return part


@dataclass
class Message:
    to_addr: str | list[str]
    subject: str
    message: str
    CCs: list[str] = field(default_factory=list)
    BCCs: list[str] = field(default_factory=list)
    attachments: list[Path] = field(default_factory=list)
    from_addr: str | None = None

    def to_msg(self, from_addr: str | None) -> MIMEMultipart:
        if self.from_addr is not None:
            from_addr = self.from_addr
        if from_addr is None:
            raise RuntimeError("I need a from-address.")

        msg = MIMEMultipart()
        msg["Subject"] = self.subject
        msg["From"] = from_addr
        if isinstance(self.to_addr, str):
            msg["To"] = self.to_addr
        else:
            msg["To"] = concatenate(self.to_addr)

        if self.CCs:
            msg["CC"] = concatenate(self.CCs)
        if self.BCCs:
            msg["BCC"] = concatenate(self.BCCs)

        msg.attach(MIMEText(self.message, "plain", _charset="utf-8"))

        return self.add_attachments(msg)

    def add_attachments(self, msg: MIMEMultipart) -> MIMEMultipart:
        for f in self.attachments:
            msg.attach(read_attachment(f))

        return msg

    def __str__(self) -> str:
        output = [f"To: {self.to_addr}", f"Subject: {self.subject}"]
        if self.CCs:
            output.append(f"CC: {self.CCs}")
        if self.BCCs:
            output.append(f"BCC: {self.BCCs}")
        if self.attachments:
            output.append(f"Attachments: {concatenate(map(str, self.attachments))}")
        return concatenate(output)
