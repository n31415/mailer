from dataclasses import dataclass
from typing import Any


@dataclass
class Entry:
    column: str

    def extract(self, row: dict[str, Any]) -> Any:
        return row[self.column]
