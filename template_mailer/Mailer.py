import contextlib
import smtplib
import ssl
from smtplib import SMTP, SMTPSenderRefused, SMTPServerDisconnected
from typing import Any, Protocol

from typing_extensions import override

from .Configuration import Configuration
from .Message import Message
from .Password import get_password


def create_server(config: Configuration) -> SMTP:
    if config.starttls:
        server = smtplib.SMTP(config.server, port=config.port)
        context = ssl.create_default_context()
        server.starttls(context=context)
    else:
        server = smtplib.SMTP_SSL(config.server, port=config.port)
    return server


class BaseMailer(Protocol):
    def send_msg(self, msg: Message, retry: bool = False) -> Any:
        ...

    def reopen(self) -> None:
        return


class Mailer(BaseMailer):
    password: str
    server: SMTP
    config: Configuration

    def __init__(self, config: Configuration):
        self.config = config

        self.password = get_password(config)
        self.server = create_server(config)
        self.server.login(self.config.user, self.password)

    def reopen(self) -> None:
        self.server = create_server(self.config)
        self.server.login(self.config.user, self.password)

    def __del__(self) -> None:
        with contextlib.suppress(smtplib.SMTPServerDisconnected):
            self.server.quit()

    @override
    def send_msg(self, msg: Message, retry: bool = False) -> Any:
        print(f"Sending {msg}")
        smtp_msg = msg.to_msg(self.config.sender)
        try:
            return self.server.send_message(smtp_msg)
        except (SMTPServerDisconnected, SMTPSenderRefused) as e:
            if retry:
                print(f"Failed with exception {e}, retrying...")
                self.reopen()
                return self.server.send_message(smtp_msg)
            print(f"Failed with exception {e}, retry not requested or already done.")
            raise


class DryMailer(BaseMailer):
    config: Configuration

    def __init__(self, config: Configuration):
        self.config = config

    @override
    def send_msg(self, msg: Message, retry: bool = False) -> Any:
        msg.to_msg(None)
        return "\n".join((f"Running dry: {msg}", msg.message))
