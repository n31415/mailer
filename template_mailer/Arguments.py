import argparse
import os
import typing
from pathlib import Path
from typing import Any, Literal, Self, TypeAlias

from pydantic import BaseModel, Field, model_validator
from pydantic.fields import FieldInfo

from template_mailer.Entry import Entry

PathLike: TypeAlias = Literal["-"] | Path


class Arguments(BaseModel):
    data: PathLike = Field(description="Input data file")
    template: Path = Field(description="Mail template file")
    wet: bool = Field(False, description="Combines --imap and --smtp")
    smtp: bool = Field(False, description="Do not dry run but really send the mails.")
    imap: bool = Field(False, description="Store the mails in the sent folder")
    mail: str | Entry = Field(
        Entry("mail"),
        description='Key for column from which to obtain the email address. Default: "mail".',
    )
    subject: str | Entry = Field(
        Entry("subject"),
        description='Key for column from which to obtain the subject. Default: "subject".',
    )
    config: str | None = Field(None, description="Section from the config file to use.")
    config_file: Path = Field(
        Path(os.environ.get("XDG_CONFIG_DIR", "~/.config")) / "mail.config",
        description="Config file to use.",
    )
    attachments: list[Path] = Field(
        [],
        description="Attachment to add to *each* mail. Can be specified multiple times.",
    )
    ccs: list[str] = Field(
        [],
        description="CCs to add to *each* mail. Can be specified multiple times.",
    )
    bccs: list[str] = Field(
        [],
        description="BCCs to add to *each* mail. Can be specified multiple times.",
    )
    sleep: None | float = Field(
        None,
        description="Time (in seconds) to sleep between mails. Some mail providers will not like sending a lot of single emails in a short interval.",
    )

    def settable_from_template(self) -> dict[str, Any]:
        return self.model_dump(include={"mail", "subject", "attachments", "bccs", "ccs"})

    @model_validator(mode="after")
    def propagate_wet(self) -> Self:
        if self.wet:
            self.imap = True
            self.smtp = True
        return self


def add_option(parser: argparse.ArgumentParser, name: str, info: FieldInfo) -> None:
    title = info.title or name
    if not title.startswith("-"):
        title = "--" + title

    nargs = "*" if typing.get_origin(info.annotation) is list else "?"

    if info.annotation is bool and info.default is False:
        parser.add_argument(title, help=info.description, dest=name, action="store_true")
    elif info.annotation is bool and info.default is True:
        parser.add_argument(title, help=info.description, dest=name, action="store_false")
    else:
        parser.add_argument(title, help=info.description, dest=name, nargs=nargs)


def add_argument(parser: argparse.ArgumentParser, name: str, info: FieldInfo) -> None:
    title = info.title or name
    nargs = "+" if typing.get_origin(info.annotation) is list else None
    parser.add_argument(name, metavar=title, help=info.description, nargs=nargs)


def parse_args() -> Arguments:
    parser = argparse.ArgumentParser()

    for name, info in Arguments.model_fields.items():
        if info.is_required():
            add_argument(parser, name, info)
        else:
            add_option(parser, name, info)

    parsed = parser.parse_args().__dict__
    return Arguments.model_validate({key: value for key, value in parsed.items() if value is not None})
