import csv
import json
import sys
from dataclasses import dataclass
from enum import Enum, auto
from io import StringIO, TextIOBase
from json import JSONDecodeError
from pathlib import Path
from typing import Any, TextIO, TypeAlias, cast

from .Arguments import PathLike

DataType: TypeAlias = list[dict[str, Any]]


class FormatHint(Enum):
    Unknown = auto()
    JSON = auto()
    CSV = auto()


@dataclass
class Data:
    content: TextIOBase
    format_hint: FormatHint


def guess_format(input_path: PathLike) -> FormatHint:
    if input_path == "-":
        return FormatHint.Unknown
    assert isinstance(input_path, Path)
    if input_path.suffix == ".csv":
        return FormatHint.CSV
    if input_path.suffix == ".json":
        return FormatHint.JSON
    return FormatHint.Unknown


def read_json(file: TextIO) -> DataType:
    return cast(DataType, json.load(file))


def read_csv(file: TextIO) -> DataType:
    dialect = csv.Sniffer().sniff(file.read(1024))
    file.seek(0)
    return list(csv.DictReader(file, dialect=dialect))


def read_data(file: TextIO, hint: FormatHint) -> DataType:
    if hint == FormatHint.Unknown:
        if not file.seekable():
            file = StringIO(file.read())
        try:
            return read_json(file)
        except JSONDecodeError:
            file.seek(0)
            return read_csv(file)
    if hint == FormatHint.JSON:
        return read_json(file)
    if hint == FormatHint.CSV:
        return read_csv(file)

    raise AssertionError(f"Unknown enum value {hint}")


def read(input_path: PathLike) -> DataType:
    hint = guess_format(input_path)
    if input_path == "-":
        return read_data(sys.stdin, hint)

    with Path(input_path).open(newline="") as f:
        return read_data(f, hint)
